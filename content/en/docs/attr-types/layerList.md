---
title: layerList
---
list of strings separated by characters from the [`layersep`](#a:layersep)
attribute (by default, colons, tabs or spaces), defining [`layer`](#a:layer)
names and implicitly numbered 1,2,...
